
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.jayway.restassured.response.Response;
import common.Apis;
import common.RestCall;
import data.CustomerDP;
import org.testng.Assert;
import org.testng.annotations.Test;
import pojo.AuthPojo;
import pojo.CustomerPojo;
import utils.Helper;

import java.util.List;

public class TestCustomer {
    String token="";
    Helper hm=new Helper();

    @Test(description = "Creation auth token with valid user",priority = -1,dataProviderClass = CustomerDP.class,dataProvider = "validdata")
    public void getAuthKey(String username,String password){
        AuthPojo ap=new AuthPojo(username,password);
        Response rs= RestCall.POSTRequest(Apis.BASEURL+Apis.AUTHAPI,hm.serialize(ap));
        Assert.assertEquals(rs.getStatusCode(),200);
        token= JsonPath.read(rs.getBody().asString(),"$.token");
    }

    @Test(description = "Get auth token with invalid users",dataProviderClass = CustomerDP.class,dataProvider = "wrongdata")
    public void getAuthInvalidUser(String username,String password){
        AuthPojo ap=new AuthPojo(username,password);
        Response rs= RestCall.POSTRequest(Apis.BASEURL+Apis.AUTHAPI,hm.serialize(ap));
        Assert.assertEquals(rs.getStatusCode(),401);
    }

    @Test(description = "This test will get all customers")
    public void getAllCustomers() throws Exception{
        Response rs= RestCall.GETRequest(Apis.BASEURL+Apis.GETALLCUSTOMERS,token);
        Assert.assertEquals(rs.getStatusCode(),200);
        Assert.assertNotNull(rs.getBody().asString());
        String mobile=JsonPath.read(rs.getBody().asString(),"$[0].phone");
        String fname=JsonPath.read(rs.getBody().asString(),"$[0].first_name");
        String lname=JsonPath.read(rs.getBody().asString(),"$[0].last_name");
        String career=JsonPath.read(rs.getBody().asString(),"$[0].career");
        Assert.assertNotNull(mobile);
        Assert.assertNotNull(fname);
        Assert.assertNotNull(lname);
        Assert.assertNotNull(career);
        //For Handel list of resonse
        ObjectMapper mp=new ObjectMapper();
        List<CustomerPojo> p1=mp.readValue(rs.getBody().asString(),new TypeReference<List<CustomerPojo>>(){});
        for(CustomerPojo cm:p1){
            System.out.println(cm.getFirst_name());
            System.out.println(cm.getLast_name());
            System.out.println(cm.getPhone());
            System.out.println(cm.getCareer());
        }

        // Will verify from DB

    }

    @Test(description = "Test customer with wrong token",dataProviderClass = CustomerDP.class,dataProvider ="wrongtoken" )
    public void getAllCustomersWithWrongToken(String token){
        Response rs= RestCall.GETRequest(Apis.BASEURL+Apis.GETALLCUSTOMERS,token);
        Assert.assertEquals(rs.getStatusCode(),401);
    }

    @Test(description = "Test customer with valid mobile number")
    public void getCustomerByMobileNumber(){
        Response rs1= RestCall.GETRequest(Apis.BASEURL+Apis.GETALLCUSTOMERS,token);
        String mobile=JsonPath.read(rs1.getBody().asString(),"$[0].phone");
        Response rs= RestCall.GETRequest(Apis.BASEURL+Apis.GETBYUSERMPHONE.replace("phone",mobile),token);
        String actualmobile=JsonPath.read(rs.getBody().asString(),"$.phone");
        Assert.assertEquals(mobile,actualmobile,"Message should be same");
        String lname=JsonPath.read(rs.getBody().asString(),"$.last_name");
        String fname=JsonPath.read(rs.getBody().asString(),"$.first_name");
        Assert.assertNotNull(lname);
        Assert.assertNotNull(fname);
        //Will validate from DB
    }

    @Test(dataProviderClass = CustomerDP.class,dataProvider ="wrongtoken",description = "Get customer by mobile with wrong token")
    public void getCustomerByMobileNumberWithWrongToken(String tokenin){
        Response rs1= RestCall.GETRequest(Apis.BASEURL+Apis.GETALLCUSTOMERS,token);
        String mobile=JsonPath.read(rs1.getBody().asString(),"$[0].phone");
        Response rs= RestCall.GETRequest(Apis.BASEURL+Apis.GETBYUSERMPHONE.replace("phone",mobile),tokenin);
        Assert.assertEquals(rs.getStatusCode(),401,"Code should be same");
    }

    @Test(description = "Get customer with invalid mobile number",dataProvider = "invalidmobiledata",dataProviderClass = CustomerDP.class)
    public void getCustomerByInvalidMobileNumber(String mobile){
        Response rs= RestCall.GETRequest(Apis.BASEURL+Apis.GETBYUSERMPHONE.replace("phone",mobile),token);
        System.out.println("Data -- "+rs.getBody().asString());
        Assert.assertEquals(rs.getBody().asString(),"");

    }
}
