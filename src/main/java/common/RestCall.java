package common;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import org.testng.Reporter;

import java.util.HashMap;

public class RestCall {

    public static Response GETRequest(String URI,String token) {
        Reporter.log("Inside GETRequest call");
        RequestSpecification requestSpecification = RestAssured.given();
        requestSpecification.header("Authorization", "Bearer "+token);
        requestSpecification.contentType(ContentType.JSON);
        Response response = requestSpecification.get(URI);
        return response;
    }

    public static Response POSTRequest(String URI,String body) {
        Reporter.log("Inside PostRequest call");
        RequestSpecification requestSpecification = RestAssured.given().body(body);
        requestSpecification.contentType(ContentType.JSON);
        Response response = requestSpecification.post(URI);
        return response;
    }

}
