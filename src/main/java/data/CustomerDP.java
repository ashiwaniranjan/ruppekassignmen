package data;

import org.testng.annotations.DataProvider;

public class CustomerDP {

    @DataProvider(name = "validdata")
    public Object[][] getData() {
        return new Object[][]{
                {"rupeek","password"}
        };
    }

    @DataProvider(name = "wrongdata")
    public Object[][] getInCorrectData() {
        return new Object[][]{
                {"abcde", "abcde"}
        };
    }

    @DataProvider(name = "invalidmobiledata")
    public Object[][] getInCorrecMobileData() {
        return new Object[][]{
                {"abcde"},{"000000"}
        };
    }

    @DataProvider(name = "wrongtoken")
    public Object[][] getWrongToken() {
        return new Object[][]{
                {"abcdeefghojjajajjajajajja"}
        };
    }

}