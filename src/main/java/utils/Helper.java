package utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;

public class Helper {
    ObjectMapper objectMapper = new ObjectMapper();


    public <T> String serialize(T object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (IOException var) {
            throw new IllegalArgumentException("Cannot serialize given object");
        }
    }

    public <T> T deserialize(String json, Class<T> clazz) {
        try {
            return objectMapper.readValue(json, clazz);
        } catch (IOException var) {
            throw new IllegalArgumentException("Cannot deserialize given object");
        }
    }
}
